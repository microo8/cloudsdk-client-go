package abbyysdk

//ABBYY Cloud SDK urls
const (
	ProcessImageURL          = "v2/processImage"
	GetTaskStatusURL         = "v2/getTaskStatus"
	ListFinishedTasksURL     = "v2/listFinishedTasks"
	SubmitImageURL           = "v2/submitImage"
	ProcessDocumentURL       = "v2/processDocument"
	DeleteTaskURL            = "v2/deleteTask"
	ProcessBarcodeFieldURL   = "v2/processBarcodeField"
	ProcessCheckmarkFieldURL = "v2/processCheckmarkField"
	ProcessTextFieldURL      = "v2/processTextField"
	ProcessFieldsURL         = "v2/processFields"
	ProcessBusinessCardURL   = "v2/processBusinessCard"
	ProcessReceiptURL        = "v2/processReceipt"
	ProcessMrzURL            = "v2/processMRZ"
	GetApplicationInfoURL    = "v2/getApplicationInfo"
	ListTasksURL             = "v2/listTasks"
)
